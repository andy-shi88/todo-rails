## README

A simple todo apps first try

### Ruby version 
- 2.3

### System dependencies
- run `bundle install` to install the dependencies

### Configuration

### Database creation
- used sqlite for this, run `rake db:migrate`

### Database initialization

### Run
- get up and running by `rails server --port 3000` and visit `localhost:3000`